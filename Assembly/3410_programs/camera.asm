.386
.MODEL FLAT

ExitProcess PROTO NEAR32 stdcall, dwExitCode:DWORD

INCLUDE io.h
INCLUDE sqrt.h
INCLUDE debug.h        	; header file for input/output

cr      EQU     0dh     ; carriage return character
Lf      EQU     0ah     ; line feed

.STACK  4096            ; reserve 4096-byte stack

.DATA                   ; reserve storage for data
xWorld 	WORD   	?
yWorld 	WORD   	?
zWorld 	WORD   	?

xPoint 	WORD   	?
yPoint 	WORD   	?
zPoint 	WORD   	?

xUp 	WORD   	?
yUp 	WORD   	?
zUp 	WORD   	?

tempX 	WORD	?
tempY 	WORD 	?
tempZ 	WORD 	?
dot1	WORD	?
dot2	WORD	?
dot3	WORD	?
dot4	WORD	?

prompt1 BYTE    "Enter the x-coordinate of the camera eyepoint:  ", 0
prompt2 BYTE    "Enter the y-coordinate of the camera eyepoint:  ", 0
prompt3 BYTE    "Enter the z-coordinate of the camera eyepoint:  ", 0

prompt4 BYTE    cr, Lf,"Enter the x-coordinate of camera look at point:  ", 0
prompt5 BYTE    "Enter the y-coordinate of camera look at point:  ", 0
prompt6 BYTE    "Enter the z-coordinate of camera look at point:  ", 0

prompt7 BYTE    cr, Lf,"Enter the x-coordinate of the camera up direction:  ", 0
prompt8 BYTE    "Enter the y-coordinate of the camera up direction:  ", 0
prompt9 BYTE    "Enter the z-coordinate of the camera up direction:  ", 0

string  	BYTE   	40 DUP (?)
leftParen 	BYTE	cr, Lf,"(",0
comma  		BYTE	",",0
rightParen  BYTE	")",0
rightParenEND  BYTE	")",cr,Lf,0
slash  		BYTE	"/",0

n  		BYTE	cr, Lf,cr, Lf,"n:",0
v  		BYTE	"v:",0
u  		BYTE	"u:",0
nX    	BYTE    11 DUP (?)
nY    	BYTE    11 DUP (?)
nZ    	BYTE    11 DUP (?)
vX    	BYTE    11 DUP (?)
vY    	BYTE    11 DUP (?)
vZ    	BYTE    11 DUP (?)
uX    	BYTE    11 DUP (?)
uY    	BYTE    11 DUP (?)
uZ    	BYTE    11 DUP (?)
temp2X 	BYTE    11 DUP (?)
temp2Y 	BYTE    11 DUP (?)
temp2Z 	BYTE    11 DUP (?)
norm   		BYTE    ?
		BYTE	cr, Lf,0

displayCords MACRO xcord,ycord,zcord
		;(     x,     y,     z)
		output 	leftParen	
		lea 	eax, xcord
		itoa 	string,[eax]
		output 	string
		
		output 	comma
		lea 	eax, ycord
		itoa 	string,[eax]
		output 	string
		
		output 	comma
		lea 	eax, zcord
		itoa 	string,[eax]
		output 	string
		output 	rightParen
		
		ENDM
		
displayN 	MACRO x1,y1,z1,x2,y2,z2
		mov     ax, x1
        sub     ax, x2		;xWorld - xPoint
        itoa    nX, ax 	
		
		mov     ax, y1
        sub     ax, y2		;Same for Y
        itoa    nY, ax		

		mov     ax, z1
        sub     ax, z2		;Same for z
        itoa    nZ, ax 
		
		dotProduct 	nX,nY,nZ,nX,nY,nZ ;Store dotProduct in dot
		sqrt 	dot1		;sqrt(dot)
		itoa 	norm, ax	;store answer in norm
		
		displayVNU n,nX,nY,nZ
		
	ENDM
	
displayV 	MACRO x1,y1,z1,x2,y2,z2
		dotProduct 	x1,y1,z1,x2,y2,z2
		
		mov		ax,dot1
		mov		dot3,ax	;store (up.n)
		neg		ax		;dot2 = -(do1)nx
		mov		dx,ax	
		atoi	x2
		imul 	dx 
		mov		dot2,ax
		
		dotProduct 	x2,y2,z2,x2,y2,z2
		
		mov		ax, dot1
		mov		dot4,ax	;store (n.n)
		mov		dx,ax
		atoi	x1
		imul 	dx		;dot1 = (dot1)upX
		
		add 	ax, dot2
		mov 	tempX, ax	;vx=dot1+dot2
		itoa 	vX,	tempX
		
		;;;;;;;;;;;;;;;;;;;;;;
		mov		ax,dot3	;now do the same for y
		neg		ax
		mov		dx,ax	;dot2 = -(dot3)ny
		atoi	y2
		imul 	dx 
		mov		dot2,ax
		
		mov		ax,dot4
		mov		dx,ax	;dot1 = (dot4)upY
		atoi	y1
		imul 	dx		
		
		add 	ax, dot2
		mov 	tempY, ax
		itoa 	vY,	tempY
		
		;;;;;;;;;;;;;;;;;;;;;
		mov		ax,dot3	;now do the same for z
		neg		ax
		mov		dx,ax	;dot2 = -(dot3)nz
		atoi	z2
		imul 	dx 
		mov		dot2,ax
		
		mov		ax,dot4
		mov		dx,ax	;dot1 = (dot4)upZ
		atoi	z1
		imul 	dx		
		
		add 	ax, dot2
		mov 	tempZ, ax
		itoa 	vZ,	tempZ
		
		;;;;;;;;;;;;;;;;;;;;
		dotProduct 	vX,vY,vZ,vX,vY,vZ ;Store dotProduct in dot1
		sqrt 	dot1		;sqrt(dot)
		itoa 	norm, ax
		
		displayVNU v,vX,vY,vZ
	ENDM
displayU 	MACRO x1,y1,z1,x2,y2,z2	;n1b v2a
		;ux=y2*z1 - z2*y1
		atoi 	y2
		mov 	dx,ax
		atoi	z1
		imul 	dx
		mov 	tempX,ax
		
		atoi 	z2
		mov 	dx,ax
		atoi	y1
		imul 	dx
		mov 	tempY,ax
		
		mov     ax, tempX
        sub     ax, tempY	
		itoa 	uX,ax
		
		;uy=z2*x1 - x2*z1
		atoi 	z2
		mov 	dx,ax
		atoi	x1
		imul 	dx
		mov 	tempX,ax
		
		atoi 	x2
		mov 	dx,ax
		atoi	z1
		imul 	dx
		mov 	tempY,ax
		
		mov     ax, tempX
        sub     ax, tempY	
		itoa 	uY,ax
		
		;uz=x2*y1 - y2*x1
		atoi 	x2
		mov 	dx,ax
		atoi	y1
		imul 	dx
		mov 	tempX,ax
		
		atoi 	y2
		mov 	dx,ax
		atoi	x1
		imul 	dx
		mov 	tempY,ax
		
		mov     ax, tempX
        sub     ax, tempY	
		itoa 	uZ,ax
		
		dotProduct 	uX,uY,uZ,uX,uY,uZ ;Store dotProduct in dot1
		sqrt 	dot1		;sqrt(dot)
		itoa 	norm, ax
		
		displayVNU u,uX,uY,uZ

	ENDM
displayVNU	MACRO VNU,x,y,z
		;VNU: 
		;(     nx/norm,     ny/norm,     nz/norm)
		output 	VNU
		output 	leftParen
		output 	x
		output 	slash
		output	norm
		
		output 	comma
		output 	y
		output 	slash
		output 	norm
		
		output 	comma
		output 	z
		output 	slash
		output 	norm
		output 	rightParenEND		
		
	ENDM
	
dotProduct 	MACRO x1,y1,z1,x2,y2,z2
		;dot = x^2 + y^2 + z^2
		atoi	x1 			;Moves x into ax
		mov		dx,ax
		atoi	x2
		imul 	dx 			;x1*x2
		mov 	tempX,ax 	;store x so we can add later
		
		atoi	y1
		mov		dx,ax
		atoi 	y2			;Do the same for y
		imul 	dx
		mov 	tempY, ax
		
		atoi 	z1
		mov 	dx,ax
		atoi 	z2			;Do the same for z
		imul 	dx
		mov 	tempZ, ax
		
		mov 	ax, tempY 	
		add 	tempX, ax	;x = x + y
		mov 	ax, tempZ
		add 	tempX, ax	;x = x + z

		mov		ax,tempX	;store in dot
		mov		dot1,ax
	
		ENDM
.CODE                      	;start of main program code
_start:
		inputW 	prompt1, xWorld
		outputW xWorld
		inputW 	prompt2, yWorld
		outputW yWorld
		inputW	prompt3, zWorld
		outputW zWorld
		
		displayCords 	xWorld,yWorld,zWorld
		
		inputW 	prompt4, xPoint
		outputW xPoint
		inputW 	prompt5, yPoint
		outputW yPoint
		inputW 	prompt6, zPoint
		outputW zPoint
		
		displayCords 	xPoint,yPoint,zPoint
		
		inputW 	prompt7, xUp
		outputW xUp
		inputW 	prompt8, yUp
		outputW yUp
		inputW 	prompt9, zUp
		outputW zUp
		
		displayCords 	xUp,yUp,zUp
		
		displayN 		xWorld,yWorld,zWorld,xPoint,yPoint,zPoint
		
		itoa 	temp2X, xUp
		itoa 	temp2Y, yUp
		itoa 	temp2Z, zUp
		
		displayV		temp2X,temp2Y,temp2Z,nX,nY,nZ
		
		displayU		nX,nY,nZ,vX,vY,vZ
		
		
        INVOKE  ExitProcess, 0  ; exit with return code 0

PUBLIC _start                   ; make entry point public

END                             ; end of source code








